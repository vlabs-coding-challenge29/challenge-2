
# Component Setup
User has to select the image from the right side of the web page and then drag to the proper location and drop it over there. Likewise, complete the whole setup of the experiment.
## Running Tests

To run tests, run the following command

step 1:
```bash
  Execute the html file 
```
step 2:

```bash
 Select the component of experiment from right side of web page
```

step 3:
```bash
 Drag it to proper location
``` 

step 4:
```bash
 Drop it to there
``` 
step 5:
```bash
  Complete the whole setup
``` 
